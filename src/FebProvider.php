<?php

namespace GillingsSPH\Feb;

use Illuminate\Support\ServiceProvider;

class FebProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(){}

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/publications/bower.json' => base_path('bower.json'),
        ], 'front_end_base');
    }//boot()

}//class FebProvider